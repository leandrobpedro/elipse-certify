# Elipse Certify
Resoluções de exercícios propostos na prova prática do Elipse E3/Power.

## Formulas.prj
* Usando fórmulas, crie temas para uma aplicação.

## HistoricoAnalogico.prj
* Crie um histórico com uma medida analógica. Para essa medida vamos gerar três mensagens de alarme (*E3Browser*) com as seguintes cores: <br>
[> 90] - Valor Muito Alto (VERMELHO) <br>
[< 10] - Valor Muito Baixo (AMARELO) <br>
[Demais Valores] - Valor Normal (VERDE)

## PrimeirosRegistrosDB.prj
* Crie um histórico. Mostre na tela os dados utilizando um *E3Browser*. Crie um botão, no evento "Click" crie um script para mostrar os três primeiros registros do *E3Browser* num `MsgBox`.

## QueryFiltroData.prj
* Em um E3Query, crie uma consulta que permita ao usuário pesquisar os dados dentro de duas datas de sua escolha, essas datas devem funcionar sem depender uma da outra. Se o usuário quiser depois de janeiro, mostre depois de janeiro, se quiser antes de março, mostre antes de março, e se ele puser as duas datas, mostre os arquivos entre janeiro e março.

## RelatorioGrafico.prj
* Crie uma tela com um E3Chart para mostrar os dados de uma medida analógica qualquer. Gere um relatório com o E3Chart mostrado na tela. Abaixo dele uma tabela com todos os valores mostrados. O período dos dados deve ser o mesmo no relatório e no E3Chart.

## SelecaoPenas.prj
* No servidor de dados crie seis TAGS, sendo duas do tipo "Interno" e quatro do tipo "DEMO". Em uma tela crie um "ComboBox" que varra a pasta dados e mostre somente os nomes das TAGS do tipo "DEMO". Insira na tela um E3Chart e um botão. No Botão, no seu evento click, crie um script para inserir uma pena de tempo real com o TAG selecionado no "ComboBox". Imprima uma mensagem avisando o usuário, caso ele tente inserir duas vezes a mesma pena.

## PowerExplorerPersonalizado.prj
* Adicione uma aba customizada ao PowerExplorer.

    > :warning: Necessário Elipse Power